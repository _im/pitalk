#!/bin/bash
echo "setting wifi..."
echo "ssid = $1"
echo "pass = $2"

read -r -d '' NETW <<- EOM
  source-directory /etc/network/interfaces.d
  
  auto wlan0

  iface lo inet loopback
  iface eth0 inet dhcp

  allow-hotplug wlan0
  iface wlan0 inet dhcp
  wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf
  iface default inet dhcp
EOM

echo -e "$NETW" | sudo tee /etc/network/interfaces

read -r -d '' WIF <<- EOM
  ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
  update_config=1

  network={
    ssid="$1"
    psk="$2"
    proto=RSN
    key_mgmt=WPA-PSK
    pairwise=CCMP
    auth_alg=OPEN
  }
EOM

echo -e "$WIF" | sudo tee /etc/wpa_supplicant/wpa_supplicant.conf
