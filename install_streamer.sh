#!/bin/bash
echo "setting autologin to console"
systemctl set-default multi-user.target
ln -fs /etc/systemd/system/autologin@.service /etc/systemd/system/getty.target.wants/getty@tty1.service

sudo apt-get --assume-yes update
sudo apt-get --assume-yes install ffmpeg
sudo apt-get --assume-yes install imagemagick

echo "creating dummy image for stream"
convert -size 16x16 xc:transparent /home/pi/image.png

echo "creating stream command"
cat > /home/pi/stream.sh <<EOF
ffmpeg -nostdin -framerate 15 -re -loop 1 -s 1x1 -i image.png  -f alsa -thread_queue_size 8096 -ac 1 -ar 44100 -i hw:1 -f flv -acodec libmp3lame -ac 2 -ar 44100 -ab 48k -vcodec libx264 -pix_fmt yuv410p -preset ultrafast -r 15 -g 30 -crf 25 -vf "scale=16x16" "rtmp://live-fra.twitch.tv/app/$1" || /home/pi/stream.sh
EOF

chmod +x /home/pi/stream.sh

echo "modifying init script"

sudo echo '/home/pi/stream.sh &' >> /home/pi/.bashrc
#live_215693368_4Sg9oDOyYvqR5tgHJaleFD0569BUys
