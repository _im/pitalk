#!/bin/bash
echo "setting autologin to console"
systemctl set-default multi-user.target
ln -fs /etc/systemd/system/autologin@.service /etc/systemd/system/getty.target.wants/getty@tty1.service

sudo apt-get --assume-yes update
sudo apt-get --assume-yes install ffmpeg
sudo apt-get --assume-yes install mpv
sudo pip install streamlink

echo "creating play_ stream command"
cat > /home/pi/play_stream.sh <<EOF
streamlink -p mpv --retry-streams 5 twitch.tv/$1 audio_only || /home/pi/play_stream.sh
EOF

chmod +x /home/pi/play_stream.sh

echo "modifying init script"

sudo echo '/home/pi/play_stream.sh' >> /home/pi/.bashrc




