mount sd card

place empty "ssh" file to boot partition

place install_streamer.sh or install_player.sh into /home/pi/ on rootfs partition

connect ethernet cable betweeen router and raspi

```ssh into pi@<IP_ADDRESS>```

inside raspi's terminal

``` sudo ./install_streamer.sh TWITCH_KEY ```
or
```sudo ./install_player.sh TWITCH_USERNAME```

only run these once or edit the end of /home/pi/.bashrc file to only have 1 line calling the stream/play script.

setup wifi access with 

``` sudo wifi_settings.sh SSID PASSWORD ```
